### SLOVNÍKY A MNOŽINY

Látka probraná v tomto materiálu se na lekci bůďto vůbec nestihla, nebo byla probrána jen zlehka.

#### Množiny - sets

Třída `set` je tedy další třída, jejíž objekty jsou schopné v sobě uchovávat další objekty. Má v mnohém stejné vlastnosti jako *matematická* množina. Každý prvek v množině je unikátní a nezáleží na jeho pořadí. Množina tak slouží zejména pro testování příslušnosti, zjišťování jaké různé prvky mám v nějaké objektu, který umožňuje uchovávat neunikátní prvky (například v listu), nebo pak pro **množinové operace** - chci zjistit co mají dva listy společného. O množinových operacích se budeme bavit za chvíli. Od Pythonu 3.7 si prvky v množině drží svojí pozici, to ale stále neznamená, že jdou indexovat.

##### Množinové operace

Operace, které můžeme provádět s množinami v matematice víceméně odpovídají tomu co můžeme dělat v Pythonu. Operace mezi množinami jdou provádět dvojím způsobem.
- Operátorem
- Metodou

Pokud známe anglický výraz pro dannou množinouvou operaci, máme v podstatě vyhráno - metoda se bude jmenovat úplně stejně.

**Sjednocení - union**

Operace sjednocení je takové množinové sčítání - spojí prvky z více množin do jedné. Operátor sjednocení je pipe `|`, metoda se jmenuje `union`.
```python
A = set([1, 2, 3])
B = set([3, 4, 5])
print('Operator:', A | B )
print('Metoda: ', A.union(B) )
```

**Průnik - intersection**

Průnik dvou množin obsahuje jen ty prvky, které mají obě množiny společné. Operátor pro průnik je `&`, metoda se jmenuje `intersection`.
```python
A = set([1, 2, 3])
B = set([3, 4, 5])
print('Operator:', A & B )
print('Metoda: ', A.intersection(B) )
```

**Rozdíl - difference**

Rozdíl dvou množin obsahuje prvky, které se nachází v první z množin, ale nenachází se ve druhé. Operátor pro rozdíl je mínus `-`, metoda se jmenuje `difference`.
```python
A = set([1, 2, 3])
B = set([3, 4, 5])
print('Operator:', A - B )
print('Metoda: ', A.difference(B) )
```
Výstupem tohoto kódu bude tedy: `{1, 2}`, protože **1** a **2** jsou prvky, které jsou v množině `A` a zároveň nejsou v množině `B`.

**Symetrický rozdíl**

Tato operaceprímý matematický ekvivalent. Jedná se rozdíl sjednocení a průniku. Výsledek tedy bude obsahovat všechny prvky, které množiny nemají společné. Operátorem je `^`, metoda se jmenuje `symmetric_difference`.
```python
A = set([1, 2, 3])
B = set([3, 4, 5])
print('Operator:', A ^ B )
print('Metoda: ', A.symmetric_difference(B) )
```

**Je podmnožinou?**

Kromě testování přislušnosti jednoho prvku v množině, můžeme testovat i jestli je nějaká množina podmnožinou jiné (jestli jedna množina *obsahuje* tu druhou). Opět můžeme použít operátor nebo metodu. Nyní máme k dispozici operátory dva - `<=` a `>=`. První se ptá jestli množina na levo je podmnožinou množiny vpravo a naopak. Metoda pak funguje tím způsobem, že jako argument předáváme množinu u které testujeme, zda-li je podmnožinou. Metoda se jmenuje `issubset`.
```python
A = {-1, 0, 1, 2, 3, 4, 5}
B = {1}
print('Operator:', A <= B )
print('Operator:', A >= B )
print('Metoda: ', A.issubset(B) )
```

**Je disjunktní?**

Python přináší ještě jednu zajímavou metodu - `isdisjoint`. Ta vrací boolean hodnotu podle toho jestli množiny mají nějaké společné prvky (vrátí `False`), nebo ne (vrátí `True`).

#### Slovníky

Slovník - **dictionary** v Pythonu, podobně jako psaný slovník, ukládá data vždy pod nějakým unikátním slovem. Tomuto slovu se v Pythonu říká **klíč**. Dictionary tedy ukládá páry `klíč - hodnota`. V odbornější hantýrce je slovník datová struktura typu **mapping** - mapuju klíč na nějakou hodnotu . Klíč v Pythonovském slovníku může být jakýkoli tvz *hashable* objekt - to znamená, že za celou dobu jeho existence ho nemáme možnost změnit (jeho hash je pořád stejný - viz porovnávání souborů pomocí md5sum). Tedy pokud to víc zkonkretizujeme, klíč může být jakýkoli immutable (neměnný) objekt. K hodnotám ve slovníku pak přistupujeme podle danného klíče. Toto je i hlavní myšlenka slovníku. Přistupovat k hodnotám pomocí nějakého unikátního identifikátoru, namísto pozice které se (někdy) může i změnit.

Prázdný slovník vytoříme pomocí funkce `dict` nebo prázdných složených závorek `{}`. Pokud chceme rovnou slovník něčím naplnit, opět máme dvě možnosti:
- složené závorky - `{'klic':'hodnota'}`
- funkce - `dict(klic=hodnota)`

Vytváření funkce přináší nevýhodu v tom, že klíč, který vytváříme, musí splňovat pravidla pro jména proměnných. Navíc obsah klíče nezle takto brát z nějaké proměné.

Na hodnoty přiřazené ke konkrétním klíčům se dotazujeme podobně jako na hodnoty na nějaké konkrétní pozici v sekvenci - pomocí hranatých závorek.
```python
d = {'klic':'hodnota', 'klic2':'jina hodnota', 'klic3': 5}
print(d['klic2'])
```
Pokud Python zadaný klíč nenajde, vyhodí chybu.

Doplnění nových párů a úprava již existujících má stejný postup.
```
d = {'klic':'hodnota', 'klic2':'jina hodnota', 'klic3': 5}
d['novy klic'] = 'nova hodnota'
d['klic3'] = 'hodnota 3'
print(d)
```

##### Metody slovníků
Jelikož Python vyhodí chybu pokud se dotazujeme na klíč, který ve slovníku není, je lepší používat metodu `get`. Tato metoda má navíc jednu zajímavou vlastnost. Její použití je celkem přímočaré:
```python
d = {'klic':'hodnota', 'klic2':'jina hodnota', 'klic3': 5}

#klic ktery je ve slovniku
print(d.get('klic'))

#klic ktery neni ve slovniku
print(d.get('klickteryneexistuje'))
```
Pokud klíč, který hledáme ve slovníku není, metoda zpátky nevrátí hodnotu (očividně), ale `None`. To co metoda vrátí, pokud nic nenajde, se však dá velmi jednoduše ovlivnit předáním dalšího pozičního argumentu. Pokud tedy například cheme aby pokud nic nenajdeme jsme zpět dostali prázdný string, vypadalo by to taktko:

```python
d = {'klic':'hodnota', 'klic2':'jina hodnota', 'klic3': 5}

print(d.get('klickteryneexistuje', ''))
```

Dále máme k dispozici tvz **views** - metody které umí ze slovníku vyštípat:
1. Pouze klíče
2. Pouze hodnoty
3. Tuply v podobě: `(klíč, hodnota)`

Metoda, která umí ze slovníku vytáhnou všechny klíče v jednom balíku se jmenuje `keys`. Vrací speciální `dict_keys` objekt, který je iterovatelný (lze ho procházet for cyklem), ale nejde ho indexovat. Samozřejmě není problém ho převést např. na list. Takto se chovají všechny objekty, které vracejí views metody. 
```python
d = {'klic':'hodnota', 'klic2':'jina hodnota', 'klic3': 5}
# Chci vypsat všechny klíče
for klic in d.keys():
    print(klic)
```

Metoda která vytáhne z balíku všechny hodnoty se jmenuje `values`.
```python
d = {'klic':'hodnota', 'klic2':'jina hodnota', 'klic3': 5}
# Chci vypsat všechny hodnoty
for klic in d.values():
    print(klic)
```

Metoda která vytáhne všechny páry, každý uložený v jednom tuple se jmenuje `items`.
```python
d = {'klic':'hodnota', 'klic2':'jina hodnota', 'klic3': 5}
# Chci vypsat všechny páry
for klic in d.items():
    print(klic)
```


