1. First of all, we want to check, whether the user entered any input
2. Only if we have an input, we can work further with it
3. We want to split the input string into 2 halves. To find the **midpoint**, we apply the **floor division**
4. After we have extracted the first and the second half of the numeric string, we can convert them into integers
5. Lastly, we want to compose a conditional statement

```python
num = input('Please, give me a number: ')

# 1
if num == '':
    print('No input provided')
    # 2
    quit()

# 3
mid_point = len(num) // 2

# 4
first_half = int(num[:mid_point])
second_half = int(num[mid_point:])

# 5
if first_half % 2 == 0 and second_half % 2 == 0:
    print('Success')
elif first_half % 2 == 0:
    print('First')
elif second_half % 2 == 0:
    print('Second')
else:
    print('Neither')
```
