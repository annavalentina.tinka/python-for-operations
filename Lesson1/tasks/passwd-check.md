Your task is to create a script called `check_start.py` that will: 

- ask the user for the secret password
- if the password given, starts with any of the lowercase 'a','e','f','q','z', print to the terminal `'Welcome!'`
- otherwise print `'The input does not match'`


Example of running the script:

```
Please enter the password: abcd
Welcome!
```

```
Please enter the password: black hand
The input does not match
```
