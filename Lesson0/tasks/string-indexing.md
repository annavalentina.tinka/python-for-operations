Create a python script named `get_day.py` that will perform the following actions.

Ask the user for the answer to the question:

```
What is the name of the fourth day in a week? 
```

Of course, the correct answer in Europe is *Thursday*. Print `True` or `False` to the terminal based on whether the answer has been correct or not. Your script should print True if the user provided any of the following values: **Thursday, THURSDAY, thursday, tHursday, etc.**

Example of running the script in terminal:
```
~/PythonBeginner/Lesson1 $ python get_day.py
What is the name of the fourth day in a week? Thursday
True
```

```
~/PythonBeginner/Lesson1 $ python get_day.py
What is the name of the fourth day in a week? Wednesday
False
```

```
~/PythonBeginner/Lesson1 $ python get_day.py
What is the name of the fourth day in a week? THURsday
True
```
