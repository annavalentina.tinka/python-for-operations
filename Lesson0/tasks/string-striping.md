Create a python script named `clean_word.py` that will remove any of the below listed characters from the end and the beginning of the string `'-hello-world. ,'`

- `','`
- `'.'` 
- `'-'`
- `' '`

Example of running the script in terminal:

```
/Users/PythonBeginner/Lesson1$ python clean_word.py
Original string: -hello-world. ,
Cleaned string: hello-world
```

