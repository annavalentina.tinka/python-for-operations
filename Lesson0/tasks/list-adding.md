Goals:
- Create empty list `candidates`.
- add new names 'Bob' and 'Ann' into the list `candidates`
- print the content of the variable `candidates` introduced by the string: `Added new names to candidates: `


Example of running the script:


```
Candidates: []
Added new names to candidates: ['Bob', 'Ann']
```
