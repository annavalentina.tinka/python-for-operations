- the operation we are about to perform is called stripping and Python provides a method with such name `str.strip'
- inside the parentheses of the `strip()` method we have write a string of characters we want to erase from the beginning and the end of the stripped string

```python
word = '-hello-world. ,'
cleaned = word.strip("-,. ")
print('Original string: ' + word)
print('Cleaned string: ' + cleaned)
```
