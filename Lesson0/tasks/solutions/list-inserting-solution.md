- insertion into a list is performed on list slices
- the trick is in that the start and the stop index of the slice have to be equal, if we do not want to erase anything from the original list
- here we want to insert 'Bob' at the index 1
- on line 10 we have to convert the list employees into a string in order we can perform the concatenation (+) operation

```python
employees = ['Frank', 'Amy', 'John', 'Kate']
employees[1:1] = ['Bob']
print('Added new names to employees:',  employees)
```
### OR (better way imo)

```python
employees = ['Frank', 'Amy', 'John', 'Kate']
print(employees)
employees.insert(1, 'Bob')
print('Added new names to employees: ', employees)
```
