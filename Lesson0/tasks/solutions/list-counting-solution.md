- to calculate the number of occurences of an item in a list or string or tuple, we can use `count()` method
- again inside parentheses, we have to specify, what item we want to count 

```python
employees = ['Ann','Bob','Fred','Ann']
ann_count = employees.count('Ann')
print('Employees:', employees)
print('The number of occurences of Ann: ' + str(ann_count))
```
