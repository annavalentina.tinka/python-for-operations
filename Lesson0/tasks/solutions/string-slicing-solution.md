We will have to use slicing for the first two operations and for the last one, we will have to specify the third argument called step inside the square brackets:

1. Extract the first five letters from the word 'tutorial'

2. Extract the last five letters from the word 'approximation' 

3. Extract every third letter from the word 'approximation' 
 

```python
first_five = 'tutorial'[:5]
print('First five letters: ' + first_five)

last_five = 'approximation'[-5:]
print('Last five letters: ' + last_five)

every_third = 'approximation'[::3]
print('Every 3rd letter: ' + every_third)
```
