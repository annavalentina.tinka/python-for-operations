- we retrieve an item at index 2 as `employees[2]`
- the trickier part however is to determine the last index, which equal to the length of the sequence - 1
- when concatenating the resulting string, all items have to be strings or converted to strings

```python
employees = ['Frank', 'Bob', 'Amy', 'John', 'Kate', 'Ann', 'Ann', 'Ann']
print('At index 2 we have:', employees[2])
last_index = len(employees) - 1
print('At index ' + str(last_index) + " we have: " + employees[last_index])
```
