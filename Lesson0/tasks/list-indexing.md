- print out the name at the index 2 introduced by the string: `At index 2 we have:  `
- print out the name at the last index introduced by the string: `At index <index_num> we have:  `

The placeholder `<index_num>` should be replaced by the index number of the last position in the list `employees`

Example of running the script:


```
Employees: ['Frank', 'Bob', 'Amy', 'John', 'Kate', 'Ann', 'Ann', 'Ann']
At index 2 we have: Amy
At index 7 we have:  Ann
```
