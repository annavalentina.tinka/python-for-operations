We have 3 email addresses and we would like to **extract only the part preceding** the @ symbol. Print each extracted string to the terminal.

Here are the emails:

- mr.reilly@gmail.com
- john55@yahoo.com
- elgringo@atlas.sk

```
~/PythonBeginner/Lesson2 $ python extract.py
mr.reilly
john55
elgringo
```
