Create a python script named `str_slicing.py` that will use indexing to:

- Extract the first five letters from the word 'tutorial'
- Print them to the terminal
- Extract the last five letters from the word 'approximation'
- Print them to the terminal
- Extract every third letter from the word 'approximation'
- Print them to the terminal

Example of running the script in terminal:

```
~/PythonBeginner/Lesson1$ python str_slicing.py
First five letters: tutor
Last five letters: ation
Every 3rd letter: aritn
```

