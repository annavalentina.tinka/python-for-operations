Examples in the table below will simulate action on the list `lst = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]`

The method are only functional with list - `lst.method(argument)`

| Operation | Syntax | Description | Example | Output | 
| :--------:|:------:|:-----------:|:-------:|:------:|
|Insertion| `.append(x)` | Appends `'x'` value at the end of the list |`lst.append('50')`| `[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 50]` |
|Insertion| `.insert(i, x)` | Inserts `'x'` value at the index `'i'` | `lst.insert(0, 420)`| `[420, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9]` |
|Insertion| `.extend(seq)` | Extends the list by another list | `lst.extend([420,2510])`| `[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 420, 2510]` |
|Item Removal| `.remove(x)` | Removes item `'x'` from the list. If `'x'` is not found, we get an error | `lst.remove(9)`| `[0, 1, 2, 3, 4, 5, 6, 7, 8]` |
|Item Removal| `.pop([i])` | Returns `'i'` and deletes it from the list. If `'i'` is not stated, the method takes the last value of the list by default | `lst.pop()` | `9; [0, 1, 2, 3, 4, 5, 6, 7, 8]` | 
|Item Removal| `.clear()` | Deletes all the items from the list. The method takes no arguments | `lst.clear()` | `[]` |
|Count| `.count(x)` | Returns count of `'x'` in the list | `lst.count('5')`| `1` |
|Sort| `.sort(key=None,reverse=False)` | Returns sorted sequence according to key and the parameter `reverse`| `lst.sort(reverse=True)`| `[9, 8, 7, 6, 5, 4, 3, 2, 1, 0]` |
|Search| `.index(x)` | Finds `'x'` in list and returns its index |`lst.index('5')`| `5` |
|Reversion| `.reverse()` | Changes the order of the list. The method takes no arguments | `lst.reverse()`| `[9, 8, 7, 6, 5, 4, 3, 2, 1, 0]` |
|Copy| `.copy()` | Returns a copy of the original list. The method takes no arguments | `lst_copy = lst.copy()`| `lst_copy = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]` |
