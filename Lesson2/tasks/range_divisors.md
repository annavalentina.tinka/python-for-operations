Výsledkem tohoto úkolu bude funkce, která bude umět zjistit, která čísla ze zadného rozsahu jsou dělitelná čísly mezi 2 - 9.

Funkce by měla brát následující argumenty:
- začátek rozsahu u kterého se zjišťuje dělitelnost
- konec rozsahu

Nakonec funkce vrátí ve vhodné datové struktuře uloženou infomaci o tom, která čísla ze zadaného rozsahu jsou dělitelná čísly mezi 2 a 9. Nějak takto:
```
zadaný rozsah: 1 - 10
2 - 2, 4, 6, 8, 10
3 - 3, 6, 9
4 - 4, 8
5 - 5, 10
6 - 6
7 - 7
8 - 8
9 - 9
10 - 10
```
