### TŘETÍ LEKCE

V této lekci si probereme jak vytvářet a používat vlastní funkce.

#### Jak to funguje?
Abysme mohli používat vlastní funkce, musíme je nejdříve **definovat**. Definicí Pythonu v podstatě říkáme: *"Následující kód si zapamatuj pod daným jménem, bude se v budoucnu spouštět"*.

Jakmile je funkce definováne, můžeme jí používat - **volat**. Funkce se volá pomocí kulatých závorek `()`.

#### Základní pojmy
- **funkce** - konkrétní funkce je v Pythonu objekt, jako všechno ostatní. V tomto ohledu je funkce `print` rovna integeru `1`
- **definice funkce** - vytvoření objektu třídy funkce
- **volání funkce** - operace, kterou můžeme udělat s objektem třídy funkce - spustí kód dané funkce.
- **parametr funkce** - vnitřní proměná funkce, která slouží pro komunikaci s vnějším světem. Do parametrů předáváme data se kterými funkce pracuje.
- **argument funkce** - data která předáváme funkci při jejím volání - argumenty se uloží do parametrů

#### Definujeme vlastní funkci
Definici funkce začínáme rezervovaným slovem **def**. Následuje její jméno a v těsném závěsu závorky ve kterých určujeme parametry. Nakonec nesmíme zapomenout na dvojtečku. Kód který má funkce spouštět je pak odsazený, podobně jako v bloku **if**.

Pokud bysme tedy chtěli vyrobit jednoduchou funkci, která vypíše součet dvou parametrů, vypadalo by to takhle:
```python
def secti(a, b):
    res = a + b
    print(res)    
```
Volání této funkce by pak mohlo vypadat například takto:
```python
secti(1, 2)
secti('Kocka ', 'leze dirou.')
secti([1,2,3], [4,5])
```
Parametry `a` a `b` v této funcki jsou **povinné**. To znamená, že při volání této funkci musíme předat přesně dva argumenty. Pozdějí si vysvětlíme, jak se s parametry pracuje.

#### Return statement
Funkce v předchozí sekci pouze vypíše součet zadaných argumentů. Ven z ní nedostaneme žádnou informaci, což nám bude ve spoustě situací na obtíž. Ideálně budeme chtít funkci, která vezme nějaké argumenty, zpracuje je a nakonec ven *vrátí* výsledek, který jsme schopni uložit, nebo s ním dál jinak pracovat. Slovo *vrátí* je tu velmi důležité. Anglicky se vrátit řekne **return** a to je také klíčové slovo, kterým řekneme co má funkce vracet ven. Pokud bysme měli přepsat funkci z předchozí sekce, aby výsledek vracela, dopadlo by to takhle:
```python
def secti(a, b):
    res = a + b
    return res
```
Volání vypadá víceméně stejně. Jen musíme brát v potaz to, že výsledek funkce niní *vrací* a je třeba ho uložit, abychom s ním dále mohli pracovat. Pokud bychom ho chtěli jen vypsat, tak můžeme volání naší funkce zabalit do volání funkce `print`.
```python
vysledek = secti(2,3)

# Toto vypíše 2x to samé
print(vysledek)
print(secti(2,3))
```

Funkce které nemají **return** statement, vracejí `None`. To je objekt třídy **NoneType**, který je podobný objektům třídy **bool**. Třída **bool** umožňuje pouze dvě hodnoty - `True` a `False`. Třída **NoneType** má pouze jednu hodnotu - `None`.
